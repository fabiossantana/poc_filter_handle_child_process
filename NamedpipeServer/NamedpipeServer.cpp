// NamedpipeServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <Windows.h>

#include <iostream>
#include <string>
#include <thread>
#include <vector>

#define INSTANCES 1
#define BUFFER_SIZE 512

bool isRunning = true;
int instance = 0;

void worker(const std::string& cmd, const std::string& input)
{
	char name[512];
	HANDLE hNamedPipe = INVALID_HANDLE_VALUE;
	HANDLE hProcess = INVALID_HANDLE_VALUE;
	sprintf(name, "\\\\.\\pipe\\mypipe_%d", instance);
	hNamedPipe = CreateNamedPipe(name, PIPE_ACCESS_INBOUND,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS,
		INSTANCES,
		BUFFER_SIZE, BUFFER_SIZE, 0, 0);

	if (hNamedPipe != INVALID_HANDLE_VALUE)
	{
		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));

		char c_cmd[512];
		sprintf(c_cmd, "pipeclient %s \"%s\" \"%s\"", name, cmd.c_str(), input.c_str());
		std::cout << c_cmd << std::endl;
		// Start the child process. 
		if (CreateProcess(NULL, c_cmd, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
		{
			DWORD error = ERROR_PIPE_LISTENING;

			if (ConnectNamedPipe(hNamedPipe, NULL) ? true : GetLastError() == ERROR_PIPE_CONNECTED)
			{
				char buffer[BUFFER_SIZE + 1]{0};
				BOOL fSuccess = FALSE;
				do
				{

				DWORD cbBytesRead = 0;
				ZeroMemory(buffer, BUFFER_SIZE + 1);
				fSuccess = ReadFile(hNamedPipe, buffer, BUFFER_SIZE, &cbBytesRead, NULL);
				std::cout << buffer;

				} while (!fSuccess);
			}
			
			std::cout << std::endl;
			
			DWORD childExit = 0;
			GetExitCodeProcess(pi.hProcess, &childExit);

			//std::cout << childExit << std::endl;

			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);

		}
		else
		{
			std::cout << "Erro na criação do processo " << GetLastError() << std::endl;
		}
		CloseHandle(hNamedPipe);
	}
	else
	{
		std::cout << "Erro na criação do pipe " << GetLastError() << std::endl;
	}

}

void ServerSimulation()
{
	std::vector<std::thread> workers;

	while (isRunning)
	{
		std::string cmd;
		//std::cout << "cmd: " << std::endl;
		std::getline(std::cin, cmd);
		if (!cmd.compare("exit"))
		{
			break;
		}
		instance++;
		workers.push_back(std::thread(worker, cmd, ""));
	}

	for (auto &worker : workers)
	{
		if (worker.joinable())
			worker.join();
	}
}

int main()
{
	ServerSimulation();
    return 0;
}

