// pipeclient.cpp : Defines the entry point for the console application.
//
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>

#include <iostream>
#include <vector>
#include <fstream>

using namespace std;


typedef DWORD(WINAPI * FP_GetModuleFileNameEx)(HANDLE hProcess, HMODULE hModule,
	LPSTR lpFilename, DWORD nSize);

static const size_t CMD_BUFFER_SIZE = 4096;


DWORD CreateProcessAndGetOutput(const  std::string& path, std::string& output, const std::string& input, int timeoutSeconds)
{
	DWORD ret = ERROR_SUCCESS;
	SECURITY_ATTRIBUTES sa = { 0, NULL, TRUE };
	STARTUPINFOA si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	CHAR tempPath[MAX_PATH] = "";
	CHAR inputTempFilePath[MAX_PATH] = "";
	CHAR outputTempFilePath[MAX_PATH] = "";
	CHAR cmd[CMD_BUFFER_SIZE] = {};
	auto ValidHandle = [](HANDLE h) { return h != NULL && h != INVALID_HANDLE_VALUE; };

	GetTempPathA(MAX_PATH, tempPath);
	GetTempFileNameA(tempPath, "zends", 0, inputTempFilePath);
	GetTempFileNameA(tempPath, "zends", 0, outputTempFilePath);
	strncpy(cmd, path.c_str(), CMD_BUFFER_SIZE);
	si.dwFlags = STARTF_USESTDHANDLES;
	{
		ofstream ofs(inputTempFilePath, ios::binary);
		ofs.write(input.c_str(), input.size());
	}
	si.hStdInput = CreateFileA(inputTempFilePath, GENERIC_READ, 0, &sa, OPEN_EXISTING, 0, NULL);
	si.hStdOutput = CreateFileA(outputTempFilePath, GENERIC_WRITE, FILE_SHARE_READ, &sa, CREATE_ALWAYS, 0, NULL);
	si.hStdError = si.hStdOutput;

	if (ValidHandle(si.hStdInput) && ValidHandle(si.hStdOutput))
	{
		if (CreateProcessA(NULL, cmd, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi))
		{
			DWORD timeout = ((DWORD)timeoutSeconds) * 1000;
			DWORD wait = WaitForSingleObject(pi.hProcess, timeout);

			if (wait == WAIT_OBJECT_0)
			{
				if (!GetExitCodeProcess(pi.hProcess, &ret))
					ret = GetLastError();
			}
			else if (wait == WAIT_TIMEOUT)
			{
				ret = ERROR_TIMEOUT;
				if (!TerminateProcess(pi.hProcess, ret))
					ret = GetLastError();
				else
					Sleep(1000); // one more time to flush the stdout
			}
			else ret = GetLastError();

			CloseHandle(pi.hThread);
			CloseHandle(pi.hProcess);
		}
		else ret = GetLastError();
	}

	if (ValidHandle(si.hStdInput))
		CloseHandle(si.hStdInput);
	if (ValidHandle(si.hStdOutput))
		CloseHandle(si.hStdOutput);

	HANDLE fileH = CreateFileA(outputTempFilePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	if (fileH == INVALID_HANDLE_VALUE)
	{
		DWORD gle = GetLastError();
		if (gle == ERROR_SHARING_VIOLATION)
		{
			// aguardamos um pouco caso o arquivo esteja em uso; problemas no XP
			Sleep(1000);
			fileH = CreateFileA(outputTempFilePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
		}
	}

	if (fileH != INVALID_HANDLE_VALUE)
	{
		if (DWORD sz = GetFileSize(fileH, NULL))
		{
			vector<char> buffer(sz);
			DWORD bytesRead = 0;
			ReadFile(fileH, &buffer[0], sz, &bytesRead, NULL);
			output = std::string(&buffer[0], bytesRead);
		}
		else output.clear();
		CloseHandle(fileH);
	}

	DeleteFileA(inputTempFilePath);
	DeleteFileA(outputTempFilePath);

	if (ret != ERROR_SUCCESS && output.empty())
		output = "ErrorToString(ret)";
	SetLastError(ret);
	return ret;
}


int main(int argc, const char** argv)
{
	HANDLE hPipe = INVALID_HANDLE_VALUE;
	DWORD ret = 0;
	std::cout << argv[1] << " " << argv[2] << std::endl;
	if (argc == 4)
	{
		hPipe = CreateFile(argv[1], GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

		if (hPipe != INVALID_HANDLE_VALUE)
		{
			std::string out, in;
			ret = CreateProcessAndGetOutput(argv[2], out, argv[3], 1000);

			DWORD cbSize = out.size();
			DWORD cbWritten = 0;

			if (!WriteFile(hPipe, out.c_str(), cbSize, &cbWritten, NULL))
			{
				CloseHandle(hPipe);
				ret = GetLastError();
			}

			if (!FlushFileBuffers(hPipe))
				ret = GetLastError();

			CloseHandle(hPipe);

		}
		else
		{
			ret = GetLastError();
		}
	}
	else
	{
		ret = -1;
	}
	
    return ret;
}

